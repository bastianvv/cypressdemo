describe('Common search', () => {
  it('Search for something in DuckDuckGo start page', () => {
    cy.visit('https://start.duckduckgo.com')
    cy.get('.search__input--adv').type('Selenium').should('have.value','Selenium').type('{enter}')
    cy.url().should('include', 'Selenium')
    cy.get('.result__title').should('exist')
  })
})

describe('Empty search', () => {
  it('Search for nothing in DuckDuckGo start page', () => {
    cy.visit('https://start.duckduckgo.com')
    cy.get('.search__input--adv').type('{enter}')
    cy.url().should('contain', 'https://duckduckgo.com')
    cy.get('.search__input--adv').should('not.have.value')
  })
})

//TODO
//* Deal in a secure way with Cross origin requests to implement Bang search assertions
/*
describe('Bang search', () => {
  it('Search for something in DuckDuckGo using Wolframalpha bang', () => {
    cy.visit('https://start.duckduckgo.com')
    cy.get('.search__input--adv').type('!wa 42').should('have.value','!wa 42').type('{enter}')
  })
})
*/